package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// * CONNECTION TO DB
func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")

	if err != nil {
		panic(err)
	}

	return session
}

// * CREATE THE CONECCTION TO THE SPECIFIC COLLECTION
var collection = getSession().DB("curso_go").C("movies")

// * METHOD TO EXECUTE RESPONSES
// * WITH MOVIE
func responseMovie(w http.ResponseWriter, status int, results Movie) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(results)
}

// * WITH ARRAY MOVIES
func responseMovies(w http.ResponseWriter, status int, results []Movie) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(results)
}

// ********************************************

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo desde mi servidor web con Go")
}

func Contact(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Esta es la ruta para agregar contactos")
}

func MovieList(w http.ResponseWriter, r *http.Request) {

	// * CREATE A VARIABLE OF TYPE MOVIE
	var results []Movie

	//* QUERY THE DB
	err := collection.Find(nil).Sort("-_id").All(&results)

	// * CHECK IF SOMETHING WENT WRONG
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Resultados", results)
	}

	responseMovies(w, 200, results)
}

func MovieShow(w http.ResponseWriter, r *http.Request) {

	// * GET DATA FROM URL
	params := mux.Vars(r)

	// * GET THE ID FROM THE PARAMS
	movieID := params["id"]

	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	// * TRANSFORM THE DATA IN A HEXADECIMAL FORMAT
	oid := bson.ObjectIdHex(movieID)
	fmt.Println(movieID)
	fmt.Println(oid)

	results := Movie{}

	err := collection.FindId(oid).One(&results)

	if err != nil {
		w.WriteHeader(404)
		return
	} else {
		responseMovie(w, 200, results)
	}

}

func MovieAdd(w http.ResponseWriter, r *http.Request) {
	// * REQUEST --> GET THE DATA FROM THE BODY
	decoder := json.NewDecoder(r.Body)

	// * CREATE A VARIABLE OF MOVIE
	var movieData Movie
	// * DECODE THE VARIBLE
	err := decoder.Decode(&movieData)

	// * IF THE DECODING FAILS
	if err != nil {
		panic(err)
	}

	// * CLEAN PROCESS OF READING THE BODY
	defer r.Body.Close()

	// * PRINT THE ARRAY
	log.Println(movieData)

	// * ADD THE MOVIE
	err = collection.Insert(movieData)

	// * VERIFY IS SOMETHING WENT WRONG
	if err != nil {
		w.WriteHeader(500)
		return
	}

	responseMovie(w, 200, movieData)
}

func MovieUpdate(w http.ResponseWriter, r *http.Request) {

	// * GET DATA FROM URL
	params := mux.Vars(r)

	// * GET THE ID FROM THE PARAMS
	movieID := params["id"]

	// * CHECKS IF THE IS HAS THE VALID HEXADECIMAL
	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	// * TRANSFORM THE DATA IN A HEXADECIMAL FORMAT
	oid := bson.ObjectIdHex(movieID)

	// * DECODE THE ELEMENT AS A JSON
	decoder := json.NewDecoder(r.Body)

	// * CREATE A VARIABLE MOVIE TYPE
	var movieData Movie

	// * PRINT THE FORMATED JSON TYPE IN THE NEW VARIABLE
	err := decoder.Decode(&movieData)

	if err != nil {
		panic(err)
		w.WriteHeader(500)
		return
	}

	defer r.Body.Close()

	document := bson.M{"_id": oid}
	change := bson.M{"$set": movieData}
	err = collection.Update(document, change)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	responseMovie(w, 200, movieData)

}

type Message struct {
	Status  string `json: "status"`
	Message string `json: "message"`
}

func (this *Message) setStatus(data string) {
	this.Status = data
}

func (this *Message) setMessage(data string) {
	this.Message = data
}

func MovieRemove(w http.ResponseWriter, r *http.Request) {

	// * GET DATA FROM URL
	params := mux.Vars(r)

	// * GET THE ID FROM THE PARAMS
	movieID := params["id"]

	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	// * TRANSFORM THE DATA IN A HEXADECIMAL FORMAT
	oid := bson.ObjectIdHex(movieID)

	err := collection.RemoveId(oid)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	// results := Message{"success", "La pelicula con ID " + movieID + "ha sido borrado correctamente."}
	message := new(Message)

	message.setStatus("success")
	message.setMessage("La pelicula con ID " + movieID + "ha sido borrado correctamente.")

	results := message
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)

}
