package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	router := NewRouter()

	// * CREATE AND LISTEN A SERVER
	server := http.ListenAndServe(":8080", router)

	fmt.Println("El servidor esta corriendo en http://localhost:8080")

	// *
	log.Fatal(server)
}
