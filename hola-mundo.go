package main

// * fmt --> PROVIDES FUNCTIONS LIKE PRINTNG INFO
// * time --> PROVIDES FUNCTIONS RELATED TO TIME
import (
	"fmt"
	// "time"
)

// type Gorra struct {
// 	marca  string
// 	color  string
// 	precio float32
// 	plana  bool
// }

func main() {
	// var gorraNegra = Gorra{
	// 	marca:  "Nike",
	// 	color:  "Negro",
	// 	precio: 25.50,
	// 	plana:  false}
	// var suma int = 8 + 9
	// var resta int = 6 - 4
	// var nombre string = "Christian"
	// var apellidos string = "Tarifeño Ramirez"

	// * ANOTHER WAY TO ASING DATA TO A VARIABLE
	// pais := "Perú"

	// var prueba bool = true

	// var decimal float32 = 3.4

	// const year int = 2020

	/**
	* ARITMETHIC OPERATORS
	 */
	var numero1 float32 = 10
	var numero2 float32 = 6

	// * time.Sleep(time.Second * <number>) --> Manages times to avoid code to be executed
	// time.Sleep(time.Second * 5)
	// * fmt.Println(<message>) --> Prints a message in the screen
	// fmt.Println("Hola Mundo desde Go con " + nombre + " " + apellidos + pais)
	// fmt.Println(suma)
	// fmt.Println(resta)
	// fmt.Println(nombre)
	// fmt.Println(prueba)
	// fmt.Println(decimal)
	// fmt.Println(year)

	calculadora(numero1, numero2)
	// fmt.Print("El resto de la división es : ")
	// fmt.Println(numero1 % numero2)

	var numero3 float32 = 44
	var numero4 float32 = 7

	calculadora(numero3, numero4)

	fmt.Println(devolverTexto())
	// fmt.Println("*****************************")
	// fmt.Println(gorraNegra)
	holaMundo()

	fmt.Print("Pedido 1 -->")
	fmt.Println(gorras(45, "€"))

	fmt.Println("************************************")

	fmt.Print("Pedido 2 --> ")
	fmt.Println(gorras(24, "€"))

	pantalon("rojo", "largo", "sin_bolsillos", "nike")

	// var peliculas [3]string

	// peliculas[0] = "La verdad duele"
	// peliculas[1] = "Ciudadano ejemplar"
	// peliculas[2] = "Gran Torino"

	// peliculas := [3]string{"La vida es Bella", "Ciudadano ejemplar", "Gran Torino"}

	// var peliculas [3][2]string
	// peliculas[0][0] = "La verdad duele"
	// peliculas[0][1] = "Mientras duermes"

	// peliculas[1][0] = "Ciudadano Ejemplar"
	// peliculas[1][1] = "El señor de los anillos"

	// peliculas[2][0] = "Gran torino"
	// peliculas[2][1] = "A todo gas"

	peliculas := []string{"La vida es Bella", "Ciudadano ejemplar", "Gran Torino", "Superman"}

	// FUNCTIONS WITH SLICES
	peliculas = append(peliculas, "Sin Limites")
	peliculas = append(peliculas, "Camp Rock")

	fmt.Println(peliculas)
	fmt.Println(len(peliculas))
	fmt.Println(peliculas[0:3])
}

func pantalon(caracteristicas ...string) {
	/*
		for <no_variable_to_insert>, <element> := range <collections>{
			fmt.Println(<element>)
		}
	*/
	for _, caracteristica := range caracteristicas {
		fmt.Println(caracteristica)
	}
}

func gorras(pedido float32, moneda string) (string, float32, string) {
	precio := func() float32 {
		return pedido * 7
	}
	return "El precio del pedido es :", precio(), moneda
}

func devolverTexto() (string, string) {
	dato1 := "Víctor "
	dato2 := "Robles"
	// THIS RETURNS STRINGS VALUES
	return dato1, dato2
}

func holaMundo() {
	fmt.Println("Hola Mundo!")
}

func operacion(n1 float32, n2 float32, op string) float32 {
	var resultado float32
	if op == "+" {
		resultado = n1 + n2
	} else if op == "-" {
		resultado = n1 - n2
	} else if op == "*" {
		resultado = n1 * n2
	} else if op == "/" {
		resultado = n1 / n2
	} else {
		resultado = 0
	}
	return resultado
}

func calculadora(numero1 float32, numero2 float32) {
	// SUMA
	fmt.Print("La suma es : ")
	fmt.Println(operacion(numero1, numero2, "+"))
	// RESTA
	fmt.Print("La resta es : ")
	fmt.Println(operacion(numero1, numero2, "-"))
	// MULTIPLICACION
	fmt.Print("La multiplicación es : ")
	fmt.Println(operacion(numero1, numero2, "*"))
	//
	fmt.Print("La división es : ")
	fmt.Println(operacion(numero1, numero2, "/"))
}
