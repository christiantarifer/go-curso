package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("Lector")

	nuevoTexto := os.Args[1] + "\n"
	// escribir := ioutil.WriteFile("ficheros.txt", nuevoTexto, 0777)

	fichero, err := os.OpenFile("ficheros.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	showError(err)

	escribir, err := fichero.WriteString(nuevoTexto)
	fmt.Println(escribir)
	showError(err)

	fichero.Close()

	texto, errorDeFichero := ioutil.ReadFile("ficheros.txt")
	showError(errorDeFichero)

	fmt.Println(string(texto))
}

func showError(e error) {
	if e != nil {
		panic(e)
	}
}
