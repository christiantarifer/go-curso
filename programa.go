package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("*** MI PROGRAMA CON GO ***")

	/*
		fmt.Println("Hola " + os.Args[1] + " bienvenido al programa en Go")

		edad, _ := strconv.Atoi(os.Args[2])
		// fmt.Println(err)
		// if edad >= 18 && edad <= 99 {
		// 	fmt.Println("Eres mayor de edad")
		// } else {
		// 	fmt.Println("Eres menor de edad o demasiado mayor")
		// }

		if edad >= 18 && edad != 25 && edad != 99 {
			fmt.Println("Eres mayor de edad o tienes 17")
		} else if edad == 25 {
			fmt.Println("Tienes 25 años")
		} else if edad == 99 {
			fmt.Println("Felicidades eres longevo")
		} else {
			fmt.Println("Eres menor de edad ")
		}
	*/

	// numero, _ := strconv.Atoi(os.Args[1])

	// if numero%2 == 0 {
	// 	fmt.Println("El número " + string(numero) + " es par")
	// } else {
	// 	fmt.Println("El número " + string(numero) + " es impar")
	// }

	// peliculas := []string{"Pelicula 1", "El club de la lucha", "A todo gas", "Gran Torino"}

	// for i := 0; i < len(peliculas); i++ {
	// 	fmt.Print(peliculas[i])
	// 	if i%2 == 0 {
	// 		fmt.Println(i, " tiene una posición par")
	// 	} else {
	// 		fmt.Println(i, " tiene una posición impar")
	// 	}
	// }

	// forEach simulation
	// for _, pelicula := range peliculas {
	// 	fmt.Println(pelicula)
	// }

	momento := time.Now()
	hoy := momento.Weekday()

	switch hoy {
	case 0:
		fmt.Println("Hoy es domingo")
	case 1:
		fmt.Println("Hoy es lunes")
	case 2:
		fmt.Println("Hoy es martes")
	case 3:
		fmt.Println("Hoy es miércoles")
	case 4:
		fmt.Println("Hoy es jueves")
	case 5:
		fmt.Println("Hoy es viernes")
	case 6:
		fmt.Println("Hoy es sábado")
	default:
		fmt.Println("Hoy es otro día de la semana")
	}

}
